---
Title: CDS resources
Author: Stéphane Guillou
Date: 2019-02-11
---

Instructions and notes for sessions held at the [Centre for Digital Scholarship](https://web.library.uq.edu.au/locations-hours/centre-digital-scholarship) (CDS).

## Description

Here you will find resources related to R, Python, Shell, Git, OpenStreetMap, QGIS and OpenRefine sessions held at the Centre for Digital Scholarship (part of the University of Queensland's Library).

### Format

Most sessions use the "live-coding" format in which the instructor and the attendees type and execute commands together.
A collaborative online pad is used to list useful links, exercises and to interact.

Files available for each course, hosted in this repository:

* Markdown notes that the instructor can use to teach, and that students can refer to after the course;
* A HTML pad export that can be used to initialise a pad (https://etherpad.wikimedia.org, https://cryptpad.fr/code and https://demo.codimd.org/ are recommended to publish);
* A text file that describes the session, useful to create an event on StudenHub.

### Quick access to course resources

| Title | Course notes | Event description | Live collaborative pad |
|:-|:-:|:-:|:-:|
| R with RStudio: getting started | [md](R/rstudio_intro/rstudio_intro.md) | [txt](R/rstudio_intro/rstudio_intro_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-rstudio) |
| R data manipulation with RStudio and dplyr: an introduction | [md](R/dplyr/dplyr.md) | [txt](R/dplyr/dplyr_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-dplyr) |
| R data visualisation with RStudio and ggplot2: an introduction | [md](R/ggplot2_intro/ggplot2_intro.md) <sup>[source](R/ggplot2_intro/ggplot2_intro.Rmd)</sup> | [txt](R/ggplot2_intro/ggplot2_intro_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-ggplot2-intro) |
| R data visualisation with RStudio and ggplot2: intermediate | [md](R/ggplot2_intermediate/ggplot2_intermediate.md) <sup>[source](R/ggplot2_intermediate/ggplot2_intermediate.Rmd)</sup> | [txt](R/ggplot2_intermediate/ggplot2_intermediate_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-ggplot2-inter) |
| R data visualisation with RStudio: heatmaps | [md](R/heatmaps/heatmaps_intermediate.md) <sup>[source](R/heatmaps_intermediate.Rmd)</sup> | [txt](R/heatmaps/heatmaps_intermediate_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-heatmaps) |
| Unix Shell: an introduction | [md](Shell/shell_intro.md) | [txt](Shell/shell_intro_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-shell) |
| Git version control for collaboration | [md](Git/git.md) | [txt](Git/git_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-git) |
| OpenRefine: an introduction to dealing with messy data | [md](OpenRefine/openrefine.md) | [txt](OpenRefine/openrefine_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-openrefine) |
| Python with Spyder: an introduction to data science | [md](Python/python_intro.md) | [txt](Python/python_intro_description.txt) | [html](https://etherpad.wikimedia.org/p/cds-python) |
| Introduction to scientific programming | [md](intro_to_programming/intro_to_programming.md) <sup>[source](intro_to_programming/intro_to_programming.Rmd)</sup> | [txt](intro_to_programming/intro_to_programming_description.txt) | [html](https://frama.link/intro_prog) |
| OpenStreetMap: contribute and use the data | [md](OSM/OpenStreetMap.md) | [txt](OSM/OpenStreetMap_description.txt) | [html](https://cryptpad.fr/pad/#/2/pad/edit/ZwpBgdie-YBBXvDE68N53u2s/) |
| next: Open Source tools for your research | ... | ... | ... |

## License

All of the information on this repository (https://gitlab.com/stragu/CDS/) is freely available under the [Creative Commons - Attribution 4.0 International Licence](https://creativecommons.org/licenses/by/4.0/). You may re-use and re-mix the material in any way you wish, without asking permission, provided you cite the original source.

Part of this repository is based on Paul Andrea Martinez's work available under the same CC-By-4.0 licence at the following URL: https://github.com/orchid00/CDS
Paula's ORCID is 0000-0002-8990-1985.

## Contributing

If you have questions about contributing to the material, please contact the CDS. You can also raise an issue or create a pull request if you spot something odd. If you would like to develop on top of this, please cite the source as mentioned above.

## Contact
 
If you want to contact the CDS for a 1-on-1 consultation, an enquiry about sessions, or any question about the programs supported by the library: cds@library.uq.edu.au.
