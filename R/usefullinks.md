# What next?

## Cheatsheets

* R and RStudio-related cheatsheets: https://www.rstudio.com/resources/cheatsheets/

## Tutorials

* _R for Data Science_, excellent free online book: http://r4ds.had.co.nz/
* _R for Reproducible Scientific Analysis_, an extensive introduction to R by The Carpentries: https://swcarpentry.github.io/r-novice-gapminder/
* `swirl` is a package that allows you to learn R interactively in an R session: https://swirlstats.com/
* RStudio's _Online Learning_ page lists useful resources: https://www.rstudio.com/online-learning/
* CRAN's basic and advanced manuals: https://cran.r-project.org/manuals.html
* Lots of quality tutorials on _STHDA_: http://www.sthda.com/english/
* Lots of quality tutorials on _Quick-R_: https://www.statmethods.net/index.html
* Lots of quality tutorials on _Cookbook for R_: http://www.cookbook-r.com/
* Lynda.com R courses: https://www.lynda.com/search?q=R (remember to sign in with your organisational portal)

### Data visualisation

* _The R Graph Gallery_ for categorised examples and code: https://www.r-graph-gallery.com/

## Questions and answers

* For R programming: http://stackoverflow.com/questions/tagged/r
* About statistics specifically: https://stats.stackexchange.com/questions/tagged/r

## Documentation

* Packages from CRAN, Bioconductor and GitHub: http://www.rdocumentation.org/
* Packages from CRAN, Bioconductor, R-Forge and GitHub: https://rdrr.io/
* Explore reviews for CRAN packages: https://crantastic.org/

## R news

* Daily news and tutorials on _R-bloggers_: https://www.r-bloggers.com/

## At UQ

* See the next sessions at the CDS: https://web.library.uq.edu.au/locations-hours/centre-digital-scholarship
* If you need an extra R/RStudio session or a 1-on-1 consultation, please contact the CDS staff to organise one for you: cds@library.uq.edu.au
* Ask questions to other researchers during the weekly _Hacky Hour_ (Tuesdays at 3 pm, Café Nano, St Lucia): https://rcc.uq.edu.au/meetups
* Meet other R users at the monthly _R Peer Group_ (QIMR Berghofer, Herston). Contact Dwan Vilcins for RSVP and questions.
* QFAB offers advanced paid workshops: https://qfab.org/training
* Contact your unit's statistician
